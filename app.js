import OS from "os";
import http from "http";

const totalmemory = OS.totalmem();
const freememory = OS.freemem();

const server = http.createServer((req, res) => {
  if (req.url === "/") {
    res.write(
      `TOTAL MEMORY::::: ${totalmemory},"FREE MEMORY:::": ${freememory}`
    );
    res.end();
  }
});

server.listen(3000);
